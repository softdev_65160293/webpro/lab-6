import { Body, Controller, Get, Param, Post, Query } from '@nestjs/common';
import { TemperatureService } from './temperature.service';

@Controller('temperature')
export class TemperatureController {
  constructor(private readonly temperatureService: TemperatureService) {}
  @Get('convert')
  covert(@Query('celsius') celsius: string) {
    return this.temperatureService.convert(parseFloat(celsius));
  }

  @Post('convert')
  covertPost(@Body('celsius') celsius: number) {
    return this.temperatureService.convert(celsius);
  }

  @Get('convert/:celsius')
  covertParam(@Param('celsius') celsius: string) {
    return this.temperatureService.convert(parseFloat(celsius));
  }
}
