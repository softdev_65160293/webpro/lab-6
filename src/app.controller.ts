import { Body, Controller, Get, Param, Post, Query, Req } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getMain(): string {
    return '<html><body><h1>Hello Main</h1></body></html>';
  }

  @Get('hello')
  getHello(): string {
    return '<html><body><h1>Hello Buu</h1></body></html>';
  }

  @Post('world')
  getWorld(): string {
    return '<html><body><h1>Hello World</h1></body></html>';
  }

  @Get('test-query')
  testquery(
    @Req() req,
    @Query('celsius') celsius: number,
    @Query('type') type: string,
  ) {
    return {
      celsius: celsius,
      type: type,
    };
  }

  @Get('test-param/:celsius')
  testparam(@Req() req, @Param('celsius') celsius: number) {
    return {
      celsius: celsius,
    };
  }

  @Post('test-body')
  testbody(@Req() req, @Body() body, @Body('celsius') celsius: number) {
    return {
      celsius: celsius,
    };
  }
}
